const mongoose = require("mongoose");

// Define the Product schema with appropriate fields and constraints
var productSchema = new mongoose.Schema({
  title: {
    type: String,
    required: true,
    trim: true,
  },
  slug: {
    type: String,
    required: true,
    unique: true,
    lowercase: true,
  },
  brand: {
    type: String,
    required: true,
  },
  description: { 
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  category: {
    type: String,
    required: true,
  },
  quantity: {
    type: Number,
    required: true,
  },
  sold: {
    type: Number,
    default: 0,
  },
  images: {
    type: Array,
  },
  ratings: [ 
    {
      star: Number,
      postedBy: { type: mongoose.Schema.Types.ObjectId, ref: "User" }, 
    },
  ],
},
{ timestamps: true });

// Export the Product model based on the Product schema
module.exports = mongoose.model("Product", productSchema); 
