const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

// Define the User schema with appropriate fields and constraints
var userSchema = new mongoose.Schema({
  // First name field with validation to ensure it is required
  firstName: {
    type: String,
    required: [true, "First name is required"]
  },
  // Last name field with validation to ensure it is required
  lastName: {
    type: String,
    required: [true, "Last name is required"]
  },
  // Email field with unique constraint and validation to ensure it is required
  email: {
    type: String,
    unique: true,
    required: [true, "Email is required"]
  },  
  // Mobile number field with validation to ensure it is required
  mobileNo: {
    type: String,
    unique: true,
    required: [true, "Mobile number is required"]
  },
  // Password field with validation to ensure it is required
  password: {
    type: String,
    required: [true, "Password is required"],
  },
  role: {
    type: String,
    default: "user",
  },
  isBlocked: {
    type: Boolean,
    default: "false",
  },
  cart: {
    type: Array,
    default: [],
  },
  address: [{type: mongoose.Schema.Types.ObjectId, ref: "Address"}],
  wishlist: [{type: mongoose.Schema.Types.ObjectId, ref: "Product"}],
  refreshToken: {
    type: String,
  },
  }, {
    timestamps: true,
 });

userSchema.pre('save', async function(next) {
  const salt = await bcrypt.genSaltSync(10);
  this.password = await bcrypt.hash(this.password, salt);
});

userSchema.methods.isPasswordMatched = async function(enteredPassword) {
  return await bcrypt.compare(enteredPassword, this.password);
}

// Export the User model based on the User schema
module.exports = mongoose.model("User", userSchema);