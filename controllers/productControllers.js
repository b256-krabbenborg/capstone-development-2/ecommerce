const Product = require("../models/productModel");
const asyncHandler = require("express-async-handler");
const slugify = require("slugify");

// Create New Product
const createProduct = asyncHandler(async(req, res) => {
	try {
		if(req.body.title) {
			req.body.slug = slugify(req.body.title);
		}
		const newProduct = await Product.create(req.body);
		res.json(newProduct);
	}	catch(error) {
		throw new Error (error);
	}
});

// Get Single Product
const getProduct = asyncHandler(async(req,res) => {
	const { id } = req.params;
	try {
		const findProduct = await Product.findById(id);
		res.json(findProduct);
	}
	catch (error) {
	throw new Error (error);
	}
});

// Get All Products
const getAllProducts = asyncHandler(async(req,res) => {
	try {
		const getAllProducts = await Product.find();
		res.json(getAllProducts);
	}
	catch (error) {
	throw new Error (error);
	}
});

// Update Product

const updateProduct = asyncHandler(async (req, res) => {
  const { id } = req.params;
  try {
    if (req.body.title) {
      req.body.slug = slugify(req.body.title);
    }
    const updateProduct = await Product.findOneAndUpdate({ _id: id }, req.body, {
      new: true,
    });
    res.json(updateProduct);
  } catch (error) {
    throw new Error(error);
  }
});


// Delete Product

const deleteProduct = asyncHandler(async (req, res) => {
  const { id } = req.params; // Extract 'id' from 'req.params'
  try {
    const deleteProduct = await Product.findOneAndDelete({ _id: id });
    res.json(deleteProduct);
  } catch (error) {
    throw new Error(error);
  }
});



module.exports = { 
	createProduct, 
	getProduct, 
	getAllProducts,
	updateProduct,
	deleteProduct,
};