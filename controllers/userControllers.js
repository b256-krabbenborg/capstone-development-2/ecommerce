const User = require('../models/userModel');
const asyncHandler = require('express-async-handler');
const { generateToken } = require("../config/jwtToken");
const validateMongoDbId = require("../utils/validateMongodbid");
const { generateRefreshToken } = require("../config/refreshToken");
const jwt = require("jsonwebtoken");

// Create User

const createUser = asyncHandler(async (req, res) => {
  const email = req.body.email;
  const findUser = await User.findOne({ email });
  if (!findUser) {
    // Check if new User
    const newUser = await User.create(req.body);
    res.json(newUser);
  } else {
    throw new Error('User already exists');
    // User already exists
  }
});

const loginUserCtrl = asyncHandler(async (req, res) => {
  const { email, password } = req.body;
  // check if user exists or not
  const findUser = await User.findOne({email});
  if(findUser && await findUser.isPasswordMatched(password)) {
    const refreshToken = await generateRefreshToken(findUser?._id);
    const updateuser = await User.findByIdAndUpdate(findUser.id, {
      refreshToken: refreshToken,
    },
    { new : true }
    );
    res.cookie('refreshToken', refreshToken, {
      httpOnly: true,
      maxAge: 72*60*60*1000,
    });
    res.json({
      _id: findUser?._id,
      firstName: findUser?.firstName,
      lastName: findUser?.lastName,
      email: findUser?.email,
      mobile: findUser?.mobileNo,
      token: generateToken(findUser?.id),
    });
  } else {
    throw new Error("Invalid Login Credentials");
  }
});

// Refresh Token

const handleRefreshToken = asyncHandler(async (req, res) => {
  const cookie = req.cookies;
  // console.log(cookie);
  if (!cookie?.refreshToken) throw new Error("No Refresh Token in Cookies");
  const refreshToken = cookie.refreshToken;
  // console.log(refreshToken);
  const user = await User.findOne({ refreshToken });
  if (!user) throw new Error("No Refresh Token Present in DB or Not Matched");
  jwt.verify(refreshToken, process.env.JWT_SECRET, (err, decoded) => {
    if (err || user.id !== decoded.id) {
      throw new Error("There is something wrong with refresh token");
    }
    const accessToken = generateToken(User?._id);
    res.json({ accessToken });
  });
});

// Logout Function

const logout = asyncHandler(async(req,res) => {
  const cookie = req.cookies;
  if(!cookie?.refreshToken) throw new Error ("No Refresh Token in Cookies");
  const refreshToken = cookie.refreshToken;
  const user = await User.findOne({refreshToken});
  if(!user) {
    res.clearCookie('refreshToken', {
      httpOnly: true,
      secure: true,
    });
    return res.sendStatus(204); // forbidden
  }
  await User.findOneAndUpdate(refreshToken, {
    refreshToken: "",
  });
  res.clearCookie('refreshToken', {
      httpOnly: true,
      secure: true,
  });
  res.sendStatus(204); // forbidden
});

// Update a user

const updateUser = asyncHandler(async(req,res) => {
  const {_id} = req.user;
  validateMongoDbId(_id);
  try {
    const user = await User.findById(_id);

    if (user) {
      user.firstName = req.body.firstName || user.firstName;
      user.lastName = req.body.lastName || user.lastName;
      user.email = req.body.email || user.email;
      user.mobileNo = req.body.mobileNo || user.mobileNo;

      const updatedUser = await user.save();
      res.json(updatedUser);
    } else {
      throw new Error("User not found");
    }
  } catch (error) {
    throw new Error(error);
  }
})


// Get all users

const getAllUsers = asyncHandler(async (req, res) => {
  try {
    const getUsers = await User.find();
    res.json(getUsers);
  } catch (error) {
    throw new Error(error);
  }
});

// Get a single user

const getUser = asyncHandler(async (req, res) => {
  const {id} = req.params;
  validateMongoDbId(id);
  try {
    const getUser = await User.findById(id);
    res.json ({
      getUser,
    })
  } catch (error) {
    throw new Error(error);
  }
});

// Delete a user

const deleteUser = asyncHandler(async (req, res) => {
  const {id} = req.params;
  try {
    const deleteUser = await User.findByIdAndDelete(id);
    res.json ({
      deleteUser,
    })
  } catch (error) {
    throw new Error(error);
  }
});

// Block user

const blockUser = asyncHandler(async(req,res) => {
  const {id} = req.params;
  validateMongoDbId(id);
  try {
    const block = await User.findByIdAndUpdate(id, 
      {
        isBlocked: true,
      }, 
      {
        new: true,
      }
    );
    res.json({
      message: "User blocked",
    });
  } catch (error) {
    throw new Error (error);
  }
});

// Unblock user

const unblockUser = asyncHandler(async(req,res) => {
  const {id} = req.params;
  validateMongoDbId(id);
  try {
    const unblock = await User.findByIdAndUpdate(id, 
      {
        isBlocked: false,
      }, 
      {
        new: true,
      }
    );
    res.json({
      message: "User has been unblocked",
    });
  } catch (error) {
    throw new Error (error);
}});

module.exports = { 
  createUser, 
  loginUserCtrl, 
  getAllUsers, 
  getUser, 
  deleteUser, 
  updateUser,
  blockUser,
  unblockUser,
  handleRefreshToken,
  logout,
};
