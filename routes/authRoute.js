const express = require('express');
const { 
	createUser, 
	loginUserCtrl, 
	getAllUsers, 
	getUser, 
	deleteUser, 
	updateUser,
	blockUser,
	unblockUser,
	handleRefreshToken,
	logout,
} = require('../controllers/userControllers');
const { authMiddleware, isAdmin } = require('../middlewares/authMiddleware');
const router = express.Router();

router.post("/register", createUser);
router.post("/login", loginUserCtrl);
router.get("/all", getAllUsers);
router.get("/refresh", handleRefreshToken);
router.get("/logout", logout);
router.get("/:id", authMiddleware, isAdmin, getUser);
router.delete("/delete/:id", deleteUser);
router.put("/update", authMiddleware, updateUser);
router.put("/block/:id", authMiddleware, isAdmin, blockUser);
router.put("/unblock/:id", authMiddleware, isAdmin, unblockUser);

module.exports = router;