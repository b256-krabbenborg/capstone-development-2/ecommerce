const express = require("express");
const router = express.Router();
const { 
	createProduct, 
	getProduct, 
	getAllProducts,
	updateProduct,
	deleteProduct,
} = require("../controllers/productControllers");

router.post("/create", createProduct);
router.get("/all", getAllProducts);
router.get("/:id", getProduct);
router.put("/update/:id", updateProduct);
router.delete("/delete/:id", deleteProduct);


module.exports = router;