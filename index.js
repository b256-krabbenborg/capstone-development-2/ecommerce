const express = require('express');
const app = express();
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const authRouter = require("./routes/authRoute");
const productRouter = require("./routes/productRoute");
const bodyParser = require('body-parser');
const { notFound, errorHandler } = require('./middlewares/errorHandler'); 
const cookieParser = require('cookie-parser');
const morgan = require('morgan');
// dbConnect();

app.use(morgan("dev"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// Connect to the MongoDB cloud database using the provided connection string
mongoose.connect("mongodb+srv://admin:admin1234@b256-krabbenborg.gfrcp6x.mongodb.net/SKINKR-API?retryWrites=true&w=majority", {
  useNewUrlParser: true,
  useUnifiedTopology: true
});

let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log(`We're connected to the cloud database`));

app.use('/user', authRouter);
app.use('/product', productRouter);

// Middlewares
app.use(notFound);
app.use(errorHandler);

// Start the server, listening on port 5000
app.listen(5000, () => console.log(`SkinKR API is now online on port 5000`));
